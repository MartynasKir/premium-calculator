package com.martynas.premiumcalculator.model;

public enum RiskType {
    FIRE("FIRE"),
    THEFT("THEFT");

    private String value;

    RiskType(String riskType) {
        this.value = riskType;
    }

    public String value() {
        return value;
    }
}

package com.martynas.premiumcalculator.model;

import java.io.Serializable;
import java.util.List;

public class Policy implements Serializable {

    private final String policyNumber;
    private final String policyStatus;
    private final List<PolicyObject> policyObjects;

    public Policy(String policyNumber, String policyStatus, List<PolicyObject> policyObjects) {
        this.policyNumber = policyNumber;
        this.policyStatus = policyStatus;
        this.policyObjects = policyObjects;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getPolicyStatus() {
        return policyStatus;
    }

    public List<PolicyObject> getPolicyObjects() {
        return policyObjects;
    }
}

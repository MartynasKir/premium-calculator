package com.martynas.premiumcalculator.model;

import java.io.Serializable;

public class PolicySubObject implements Serializable {

    private final String subObjectName;
    private final double sumInsured;
    private final String riskType;

    public PolicySubObject(String subObjectName, double sumInsured, String riskType) {
        this.subObjectName = subObjectName;
        this.sumInsured = sumInsured;
        this.riskType = riskType;
    }

    public String getSubObjectName() {
        return subObjectName;
    }

    public double getSumInsured() {
        return sumInsured;
    }

    public String getRiskType() {
        return riskType;
    }
}

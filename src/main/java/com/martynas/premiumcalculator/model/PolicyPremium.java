package com.martynas.premiumcalculator.model;

import java.io.Serializable;

public class PolicyPremium implements Serializable {

    private final String policyNumber;
    private final double policyPremium;

    public PolicyPremium(String policyNumber, double policyPremium) {
        this.policyNumber = policyNumber;
        this.policyPremium = policyPremium;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public double getPolicyPremium() {
        return policyPremium;
    }
}

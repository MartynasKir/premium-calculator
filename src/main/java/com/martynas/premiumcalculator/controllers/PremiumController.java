package com.martynas.premiumcalculator.controllers;

import com.martynas.premiumcalculator.model.Policy;
import com.martynas.premiumcalculator.model.PolicyPremium;
import com.martynas.premiumcalculator.services.PremiumService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/premium")
public class PremiumController {

    private static final Logger LOG = LoggerFactory.getLogger(PremiumController.class);
    private final PremiumService premiumService;

    public PremiumController(PremiumService premiumService) {
        this.premiumService = premiumService;
    }

    @PostMapping(value = "/calculate")
    public PolicyPremium calculatePremium(@RequestBody Policy policy) {
        LOG.info("Received policy id: {}", policy.getPolicyNumber());
        return premiumService.calculatePremium(policy);
    }

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<Exception> handleAllExceptions(RuntimeException ex) {
        return new ResponseEntity<Exception>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

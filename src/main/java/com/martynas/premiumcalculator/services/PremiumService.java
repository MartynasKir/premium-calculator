package com.martynas.premiumcalculator.services;

import com.martynas.premiumcalculator.helpers.MathUtils;
import com.martynas.premiumcalculator.model.Policy;
import com.martynas.premiumcalculator.model.PolicyObject;
import com.martynas.premiumcalculator.model.PolicyPremium;
import com.martynas.premiumcalculator.model.PolicySubObject;
import com.martynas.premiumcalculator.model.RiskType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PremiumService {

    private static final Logger LOG = LoggerFactory.getLogger(PremiumService.class);
    private static final double RATE_FIRE_STANDARD = 0.014;
    private static final double RATE_FIRE_INCREASED = 0.024;
    private static final double RATE_THEFT_STANDARD = 0.11;
    private static final double RATE_THEFT_DECREASED = 0.05;
    private static final double INSURANCE_SUM_FIRE_THRESHOLD = 100;
    private static final double INSURANCE_SUM_THEFT_THRESHOLD = 15;

    public PolicyPremium calculatePremium(Policy policy) {
        double premiumFire = 0.0;
        double premiumTheft = 0.0;

        for (PolicyObject policyObject : policy.getPolicyObjects()) {
            premiumFire += getPremiumFire(policyObject);
            premiumTheft = +getPremiumTheft(policyObject);
        }
        LOG.info("Premium FIRE: '{}', Premium THEFT: '{}' ", premiumFire, premiumTheft);
        double policyPremium = premiumFire + premiumTheft;
        LOG.info("Calculated premium: {}", policyPremium);

        return new PolicyPremium(policy.getPolicyNumber(), policyPremium);
    }

    private double getPremiumFire(PolicyObject policyObject) {
        double sumInsuredFire = getSumInsuredFire(policyObject);
        double premiumFire = 0.0;

        if (sumInsuredFire <= 0) {
            LOG.info("No fire insurance found in policy. Premium for risk type FIRE: {}", premiumFire);
            return premiumFire;
        }
        if (sumInsuredFire > INSURANCE_SUM_FIRE_THRESHOLD) {
            LOG.info("Fire insurance SUM is more than '{}', increated rate applied: '{}'",
                    INSURANCE_SUM_FIRE_THRESHOLD, RATE_FIRE_INCREASED);
            premiumFire = sumInsuredFire * RATE_FIRE_INCREASED;
        } else {
            LOG.info("Standard Fire insurance rate applied: '{}'", RATE_FIRE_STANDARD);
            premiumFire = sumInsuredFire * RATE_FIRE_STANDARD;
        }
        return MathUtils.roundDouble(premiumFire);
    }

    private double getSumInsuredFire(PolicyObject policyObject) {
        double sumInsuredFire = 0.0;
        for (PolicySubObject policySubObject : policyObject.getSubObjects()) {
            if (policySubObject.getRiskType().toUpperCase().equals(RiskType.FIRE.value())) {
                sumInsuredFire += policySubObject.getSumInsured();
            }
        }
        return sumInsuredFire;
    }

    private double getPremiumTheft(PolicyObject policyObject) {
        double sumInsuredTheft = getSumInsuredTheft(policyObject);
        double premiumTheft = 0.0;

        if (sumInsuredTheft <= 0) {
            LOG.info("No theft insurance found in policy. Premium for risk type THEFT: {}", premiumTheft);
            return premiumTheft;
        }
        if (sumInsuredTheft >= INSURANCE_SUM_THEFT_THRESHOLD) {
            LOG.info("Theft insurance SUM is more than '{}', decreased rate applied: '{}'",
                    INSURANCE_SUM_THEFT_THRESHOLD, RATE_THEFT_DECREASED);
            premiumTheft = sumInsuredTheft * RATE_THEFT_DECREASED;
        } else {
            LOG.info("Standard Theft insurance rate applied: '{}'", RATE_THEFT_STANDARD);
            premiumTheft = sumInsuredTheft * RATE_THEFT_STANDARD;
        }
        return MathUtils.roundDouble(premiumTheft);
    }

    private double getSumInsuredTheft(PolicyObject policyObject) {
        double sumInsuredTheft = 0.0;
        for (PolicySubObject policySubObject : policyObject.getSubObjects()) {
            if (policySubObject.getRiskType().toUpperCase().equals(RiskType.THEFT.value())) {
                sumInsuredTheft += policySubObject.getSumInsured();
            }
        }
        return sumInsuredTheft;
    }
}

package com.martynas.premiumcalculator.helpers;

public class MathUtils {

    public static double roundDouble(double number) {
        return Math.round(number * 100.00) / 100.00;
    }
}

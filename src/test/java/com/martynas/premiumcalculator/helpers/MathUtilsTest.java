package com.martynas.premiumcalculator.helpers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MathUtilsTest {

    @Test
    public void testRoundFunction() {
        double expectedValue = 17.13;
        double actualValue = MathUtils.roundDouble(17.1255774);

        Assertions.assertEquals(expectedValue, actualValue);
    }
}

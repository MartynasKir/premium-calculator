package com.martynas.premiumcalculator.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.martynas.premiumcalculator.model.Policy;
import com.martynas.premiumcalculator.model.PolicyObject;
import com.martynas.premiumcalculator.model.PolicySubObject;
import com.martynas.premiumcalculator.services.PremiumService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {PremiumController.class, PremiumService.class})
@WebMvcTest
public class PremiumControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PremiumService premiumService;

    @Test
    public void givenValidPolicy_calculateMethod() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/premium/calculate")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"policyNumber\":\"1234\","
                        + "\"policyStatus\":\"new\","
                        + "\"policyObjects\":["
                        + "{\"objectName\":\"house\","
                        + "\"subObjects\":["
                        + "{\"subObjectName\":\"phone\",\"sumInsured\":250.00,\"riskType\":\"Theft\"},"
                        + "{\"subObjectName\":\"tv\",\"sumInsured\":650.00,\"riskType\":\"Fire\"}"
                        + "]}]}"
                )).andExpect(status().isOk());

        verify(premiumService).calculatePremium(any(Policy.class));
    }

    private Policy getPolicyObject() {
        PolicySubObject policySubObject1 = new PolicySubObject("tv", 0, "fire");
        PolicySubObject policySubObject2 = new PolicySubObject("phone", 0, "theft");
        PolicyObject policyObject1 = new PolicyObject("House", List.of(policySubObject1, policySubObject2));
        Policy policy = new Policy("123", "NEW", List.of(policyObject1));
        return policy;
    }
}

package com.martynas.premiumcalculator.services;

import com.martynas.premiumcalculator.model.Policy;
import com.martynas.premiumcalculator.model.PolicyObject;
import com.martynas.premiumcalculator.model.PolicyPremium;
import com.martynas.premiumcalculator.model.PolicySubObject;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PremiumServiceTest {

    PremiumService premiumService = new PremiumService();

    @Test
    void givenOnePolicyObjectWithTwoSubObjects1_calculatePremium() {
        PolicySubObject policySubObject1 = new PolicySubObject("tv", 100, "fire");
        PolicySubObject policySubObject2 = new PolicySubObject("phone", 4, "theft");

        PolicyPremium premium = premiumService.calculatePremium(getPolicyObject(policySubObject1, policySubObject2));
        double actualResult = Math.round(premium.getPolicyPremium() * 100.00) / 100.00;
        Assertions.assertEquals(1.84, actualResult);
    }

    @Test
    void givenOnePolicyObjectWithTwoSubObjects2_calculatePremium() {
        PolicySubObject policySubObject1 = new PolicySubObject("tv", 100, "fire");
        PolicySubObject policySubObject2 = new PolicySubObject("phone", 8, "theft");

        PolicyPremium premium = premiumService.calculatePremium(getPolicyObject(policySubObject1, policySubObject2));
        double actualResult = Math.round(premium.getPolicyPremium() * 100.00) / 100.00;
        Assertions.assertEquals(2.28, actualResult);
    }

    @Test
    void givenOnePolicyObjectWithTwoSubObjects3_calculatePremium() {
        PolicySubObject policySubObject1 = new PolicySubObject("tv", 101, "fire");
        PolicySubObject policySubObject2 = new PolicySubObject("phone", 4, "theft");

        PolicyPremium premium = premiumService.calculatePremium(getPolicyObject(policySubObject1, policySubObject2));
        double actualResult = Math.round(premium.getPolicyPremium() * 100.00) / 100.00;
        Assertions.assertEquals(2.86, actualResult);
    }

    @Test
    void givenOnePolicyObjectWithTwoSubObjects4_calculatePremium() {
        PolicySubObject policySubObject1 = new PolicySubObject("tv", 101, "fire");
        PolicySubObject policySubObject2 = new PolicySubObject("phone", 8, "theft");

        PolicyPremium premium = premiumService.calculatePremium(getPolicyObject(policySubObject1, policySubObject2));
        double actualResult = Math.round(premium.getPolicyPremium() * 100.00) / 100.00;
        Assertions.assertEquals(3.3, actualResult);
    }

    @Test
    void givenOnePolicyObjectWithTwoSubObjects5_calculatePremium() {
        PolicySubObject policySubObject1 = new PolicySubObject("tv", 500, "fire");
        PolicySubObject policySubObject2 = new PolicySubObject("phone", 102.51, "theft");

        PolicyPremium premium = premiumService.calculatePremium(getPolicyObject(policySubObject1, policySubObject2));
        double actualResult = Math.round(premium.getPolicyPremium() * 100.00) / 100.00;
        Assertions.assertEquals(17.13, actualResult);
    }

    @Test
    void givenOnePolicyWithoutSumInsured_calculatePremiumZero() {
        PolicySubObject policySubObject1 = new PolicySubObject("tv", 0, "fire");
        PolicySubObject policySubObject2 = new PolicySubObject("phone", 0, "theft");

        PolicyPremium premium = premiumService.calculatePremium(getPolicyObject(policySubObject1, policySubObject2));
        double actualResult = Math.round(premium.getPolicyPremium() * 100.00) / 100.00;
        Assertions.assertEquals(0.00, actualResult);
    }

    private Policy getPolicyObject(PolicySubObject policySubObject1, PolicySubObject policySubObject2) {
        PolicyObject policyObject1 = new PolicyObject("House", List.of(policySubObject1, policySubObject2));
        return new Policy("123", "NEW", List.of(policyObject1));
    }
}

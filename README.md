# Premium calculator

Purpose: calculate insurance policy premium for given policy object


####Request example:
POST to: http://localhost:8080/api/v1/premium/calculate
```json
{
    "policyNumber":"1234",
    "policyStatus":"new",
    "policyObjects":[
        {
            "objectName":"house",
            "subObjects":[
                {
                    "subObjectName":"phone",
                    "sumInsured":102.51,
                    "riskType":"Theft"
                },
                {
                    "subObjectName":"tv",
                    "sumInsured":500.00,
                    "riskType":"Fire"
                }   
            ]
        }
    ]
}
```

#### Response example:
```json
{
    "policyNumber": "1234",
    "policyPremium": 17.13
}
```
